'use strict';

(function() {
	// Scenarios Controller Spec
	describe('Scenarios Controller Tests', function() {
		// Initialize global variables
		var ScenariosController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Scenarios controller.
			ScenariosController = $controller('ScenariosController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Scenario object fetched from XHR', inject(function(Scenarios) {
			// Create sample Scenario using the Scenarios service
			var sampleScenario = new Scenarios({
				name: 'New Scenario'
			});

			// Create a sample Scenarios array that includes the new Scenario
			var sampleScenarios = [sampleScenario];

			// Set GET response
			$httpBackend.expectGET('scenarios').respond(sampleScenarios);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.scenarios).toEqualData(sampleScenarios);
		}));

		it('$scope.findOne() should create an array with one Scenario object fetched from XHR using a scenarioId URL parameter', inject(function(Scenarios) {
			// Define a sample Scenario object
			var sampleScenario = new Scenarios({
				name: 'New Scenario'
			});

			// Set the URL parameter
			$stateParams.scenarioId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/scenarios\/([0-9a-fA-F]{24})$/).respond(sampleScenario);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.scenario).toEqualData(sampleScenario);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Scenarios) {
			// Create a sample Scenario object
			var sampleScenarioPostData = new Scenarios({
				name: 'New Scenario'
			});

			// Create a sample Scenario response
			var sampleScenarioResponse = new Scenarios({
				_id: '525cf20451979dea2c000001',
				name: 'New Scenario'
			});

			// Fixture mock form input values
			scope.name = 'New Scenario';

			// Set POST response
			$httpBackend.expectPOST('scenarios', sampleScenarioPostData).respond(sampleScenarioResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Scenario was created
			expect($location.path()).toBe('/scenarios/' + sampleScenarioResponse._id);
		}));

		it('$scope.update() should update a valid Scenario', inject(function(Scenarios) {
			// Define a sample Scenario put data
			var sampleScenarioPutData = new Scenarios({
				_id: '525cf20451979dea2c000001',
				name: 'New Scenario'
			});

			// Mock Scenario in scope
			scope.scenario = sampleScenarioPutData;

			// Set PUT response
			$httpBackend.expectPUT(/scenarios\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/scenarios/' + sampleScenarioPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid scenarioId and remove the Scenario from the scope', inject(function(Scenarios) {
			// Create new Scenario object
			var sampleScenario = new Scenarios({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Scenarios array and include the Scenario
			scope.scenarios = [sampleScenario];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/scenarios\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleScenario);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.scenarios.length).toBe(0);
		}));
	});
}());