'use strict';

// Configuring the Articles module
angular.module('scenarios').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Scenarios', 'scenarios', 'dropdown', '/scenarios(/create)?');
		Menus.addSubMenuItem('topbar', 'scenarios', 'List Scenarios', 'scenarios');
		Menus.addSubMenuItem('topbar', 'scenarios', 'New Scenario', 'scenarios/create');
	}
]);