'use strict';

//Setting up route
angular.module('scenarios').config(['$stateProvider',
	function($stateProvider) {
		// Scenarios state routing
		$stateProvider.
		state('listScenarios', {
			url: '/scenarios',
			templateUrl: 'modules/scenarios/views/list-scenarios.client.view.html'
		}).
		state('createScenario', {
			url: '/scenarios/create',
			templateUrl: 'modules/scenarios/views/create-scenario.client.view.html'
		}).
		state('viewScenario', {
			url: '/scenarios/:scenarioId',
			templateUrl: 'modules/scenarios/views/view-scenario.client.view.html'
		}).
		state('editScenario', {
			url: '/scenarios/:scenarioId/edit',
			templateUrl: 'modules/scenarios/views/edit-scenario.client.view.html'
		});
	}
]);