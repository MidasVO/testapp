'use strict';

// Scenarios controller
angular.module('scenarios').controller('ScenariosController', ['$scope', '$rootScope', '$stateParams', '$q', '$location', '$http', 'Authentication', 'Scenarios', 'Tests', 'Requests',
	function ($scope, $rootScope, $stateParams, $q, $location, $http, Authentication, Scenarios, Tests, Requests) {
		'use strict';

		$scope.authentication = Authentication;
		var tests = [];
		$rootScope.listOfTests = {
			tests: []
		};

        // maak array aan en bij add vul in met data van rootScope

        // Create new Scenario
        $scope.create = function () {
        	tests = $rootScope.listOfTests.tests;

            // Create new Scenario object
            var scenario = new Scenarios({
            	name: this.name,
            	description: this.description,
            	sequence: this.sequence,
            	testSet: tests
            });

            // Redirect after save
            scenario.$save(function (response) {
            	$location.path('scenarios/' + response._id);

                // Clear form fields
                $scope.name = '';
            }, function (errorResponse) {
            	$scope.error = errorResponse.data.message;
            });
        };

        // Remove existing Scenario
        $scope.remove = function (scenario) {
        	var i;
        	if (scenario) {
        		scenario.$remove();

        		for (i in $scope.scenarios) {
        			if ($scope.scenarios[i] === scenario) {
        				$scope.scenarios.splice(i, 1);
        			}
        		}
        	} else {
        		$scope.scenario.$remove(function () {
        			$location.path('scenarios');
        		});
        	}
        };

        // Update existing Scenario
        $scope.update = function () {
        	var scenario = $scope.scenario;

        	scenario.$update(function () {
        		$location.path('scenarios/' + scenario._id);
        	}, function (errorResponse) {
        		$scope.error = errorResponse.data.message;
        	});
        };

        // Find a list of Scenarios
        $scope.find = function () {
        	$scope.scenarios = Scenarios.query();
        };

        /**
         * runScenario goes through the testSet concurrently and pushes the result from the assertion to an Array.
         * @return {[type]} [description]
         */
         $scope.runScenario = function () {
         	var i;
         	var scenario = $scope.scenario;
         	var testSet = scenario.testSet;
         	console.log(scenario.sequence);
         	var amountTests = testSet.length;
         	var request, test, currentResponse, body, assertionResult;

         	var promises = [];
         	var responses = [];
         	var results = [];


         	angular.forEach(testSet, function (val) {
         		request = angular.fromJson(val.test.request);
         		var promise = $http.post('/requests/do/' + request._id).then(function (response) {
         			responses.push(response);
         		});
         		promises.push(promise);
         	});
         	$q.all(promises).then(function () {
         		for (i = 0; i < responses.length; i++) {
         			var currentAssertion = testSet[i].test;
                    currentResponse = angular.fromJson(responses[i].data); // what we get back from the server
                    var currentResponseBody = angular.fromJson(responses[i].data.body)[0];
                    $scope.testInfo = currentAssertion;

                    var expectation, operator, answer;
                    expectation = currentAssertion.expectation;
                    operator = currentAssertion.operator;


                    if (currentAssertion.expression === 'statusCode') { // not sure if still needed with new calc function but let's not break it now
                    	answer = currentResponse.statusCode;
                        assertionResult = eval(expectation + operator + answer); // boolean

                    } else { // Search the body for the one.
                    	$scope.testAnswer = currentResponseBody[currentAssertion.expression];
                    	answer = $scope.testAnswer;
                        assertionResult = $scope.calc(expectation, operator, answer); //boolean
                    }
                    results.push({
                    	testResult: assertionResult,
                    	testInfo: currentAssertion
                    });
                }
            });

$scope.scenarioResults = angular.fromJson(results);

};


        $scope.calc = function (exp, op, ans) { // maybe a catch for every operator?
        	var r;
        	switch (op) {

        		case '===':
        		if (exp === ans) {
        			r = true;
        		} else {
        			r = false;
        		}
        		break;

        		case '==':
        		if (exp == ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '<':
        		if (exp < ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '<=':
        		if (exp <= ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '!=':
        		if (exp != ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '!==':
        		if (exp !== ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '>':
        		if (exp > ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;

        		case '>=':
        		if (exp >= ans) {
        			r = true;
        		} else {
        			r = false;
        		}

        		break;
        	}
        	return r;
        };

        $scope.add = function () {
        	console.log('ScenariosController: ' + $rootScope.listOfTests.tests[0].test.description);
        };

        $scope.done = function () {
        	console.log('done');
        };

        // Find existing Scenario
        $scope.findOne = function () {
        	$scope.scenario = Scenarios.get({
        		scenarioId: $stateParams.scenarioId
        	});
        };
    }
    ]);