'use strict';

// Tests controller
angular.module('tests').controller('TestsController', ['$scope', '$rootScope', '$stateParams', '$location', '$http', 'Authentication', 'Tests', 'Requests', 'Scenarios',
	function($scope, $rootScope, $stateParams, $location, $http, Authentication, Tests, Requests, Data, Scenarios) {
		$scope.authentication = Authentication;
		
		$scope.create = function() {
			// Create new Test object			
			var test = new Tests ({
				name: this.name,
				expression: this.expression,
				operator: this.operator,
				expectation: this.expectation,
				description: this.description,
				request: this.request
			});

			// Redirect after save
			test.$save(function(response) {
				$location.path('tests/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Test
		$scope.remove = function(test) {
			if ( test ) { 
				test.$remove();

				for (var i in $scope.tests) {
					if ($scope.tests [i] === test) {
						$scope.tests.splice(i, 1);
					}
				}
			} else {
				$scope.test.$remove(function() {
					$location.path('tests');
				});
			}
		};

		// Update existing Test
		$scope.update = function() {
			var test = $scope.test;

			test.$update(function() {
				$location.path('tests/' + test._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Tests
		$scope.find = function() {
			$scope.tests = Tests.query();

		};
		/**
		 * Adds the scenario to the rootScope
		 * @param {[type]} test [description]
		 */
		 $scope.addToScenario = function(test){
		 	$rootScope.listOfTests.tests.push({test});
		 };
		 /**
		  * [runTest description]
		  * @return {[type]} [description]
		  */
	 	$scope.runTest = function () {
		  	var request, thisTest, assertionResult, thisExpression, resultBody;
		  	request = angular.fromJson($scope.test.request);
		  	thisTest = $scope.test;
		  	$http.post('/requests/do/' + request._id).then(function (result) {
		  		if(angular.fromJson(result.data.body)[0] === undefined) { // probably yahoo
			  		$scope.bodyText = angular.fromJson(result.query);
			  		resultBody = angular.fromJson(result.query);
			  		$scope.statusCode = result.data.statusCode;
			  		$scope.headers = 'Headers: ' + result.data.headers;

		  		} else {
			  		$scope.bodyText = angular.fromJson(result.data.body)[0];
			  		resultBody = angular.fromJson(result.data.body)[0];
			  		$scope.statusCode = result.data.statusCode;
			  		$scope.headers = 'Headers: ' + result.data.headers;
		  		}
		  		thisExpression = thisTest.expression;
		  		var expectation, operator, answer;

		  		operator = $scope.test.operator;
		  		expectation = $scope.test.expectation;



		  		if(thisTest.expression === 'statusCode') { // not sure if still needed with new calc function but let's not break it now
					$scope.testAnswer = eval('$scope.' + thisExpression); //only finds statusCode
					answer = $scope.testAnswer;
					assertionResult = eval(expectation + operator + answer);

		  		} else { // Search the body for the one.
					$scope.testAnswer = resultBody[thisExpression];
					answer = $scope.testAnswer;
			  		assertionResult = $scope.calc(expectation, operator, answer);

		  		}
				$scope.result = 'The test came back: ' + assertionResult;
			});
		};

		$scope.calc = function (exp, op, ans) { // maybe a catch for every operator?
			var r;
			console.log(op);
			switch(op) {

				case '===':
					if(exp===ans) { r = true; } else { r = false; }
				break;

				case '==':
					if(exp==ans) { r = true; } else { r = false; }

				break;

				case '<':
					if(exp<ans) { r = true; } else { r = false; }

				break;

				case '<=':
					if(exp<=ans) { r = true; } else { r = false; }

				break;

				case '!=':
					if(exp!=ans) { r = true; } else { r = false; }

				break;

				case '!==':
					if(exp!==ans) { r = true; } else { r = false; }

				break;

				case '>':
					if(exp>ans) { r = true; } else { r = false; }

				break;

				case '>=':
					if(exp>=ans) { r = true; } else { r = false; }

				break;
			}
			return r;
		};


		$scope.findRequests = function () {
			$scope.requests = Requests.query();
		};
		// Find existing Test
		$scope.findOne = function() {
			$scope.test = Tests.get({ 
				testId: $stateParams.testId
			});
		};
	}
	]);