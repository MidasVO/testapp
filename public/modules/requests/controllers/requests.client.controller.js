'use strict';

// Requests controller
angular.module('requests').controller('RequestsController', ['$scope', '$http', '$stateParams', '$location', 'Authentication', 'Requests',
	function($scope, $http, $stateParams, $location, Authentication, Requests) {
		$scope.authentication = Authentication;
		var savedParams;
		$scope.params = [];
		$scope.paramsList = [];
		// Create new Request
		$scope.create = function() {
			// Create new Request object
			savedParams = $scope.paramsList;
			console.log('savedparams: ' + savedParams);
			var request = new Requests ({
				name: this.name,
				hostname: this.hostname,
				port: this.port,
				path: this.path,
				description: this.description,
				method: this.method,
				parameters: savedParams
			});

			// Redirect after save
			request.$save(function(response) {
				$location.path('requests/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Request
		$scope.remove = function(request) {
			if ( request ) { 
				request.$remove();

				for (var i in $scope.requests) {
					if ($scope.requests [i] === request) {
						$scope.requests.splice(i, 1);
					}
				}
			} else {
				$scope.request.$remove(function() {
					$location.path('requests');
				});
			}
		};

		// Update existing Request
		$scope.update = function() {
			var request = $scope.request;

			request.$update(function() {
				$location.path('requests/' + request._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Requests
		$scope.find = function() {
			$scope.requests = Requests.query();
		};

		// Call the server to make Request
		$scope.makeRequest = function() {
			var request = $scope.request;
			console.log(request);
			$http.post('/requests/do/' + request._id).then(function (result) {
				$scope.bodyText = result.data.body;
				$scope.statusCode = 'Status Code: ' + result.data.statusCode;
				$scope.headers = 'Headers: ' + result.data.headers;
			});

		};

		$scope.addParameters = function (params) {
			var currentParams = { name: params.name, value: params.value};
			var jsonParams = JSON.stringify(currentParams);
			$scope.paramsList.push(currentParams);			
			console.log($scope.paramsList);
		};

		// Find existing Request
		$scope.findOne = function() {
			$scope.request = Requests.get({ 
				requestId: $stateParams.requestId
			});
		};
	}
]);