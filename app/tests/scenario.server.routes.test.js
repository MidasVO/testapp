'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Scenario = mongoose.model('Scenario'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, scenario;

/**
 * Scenario routes tests
 */
describe('Scenario CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Scenario
		user.save(function() {
			scenario = {
				name: 'Scenario Name'
			};

			done();
		});
	});

	it('should be able to save Scenario instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Scenario
				agent.post('/scenarios')
					.send(scenario)
					.expect(200)
					.end(function(scenarioSaveErr, scenarioSaveRes) {
						// Handle Scenario save error
						if (scenarioSaveErr) done(scenarioSaveErr);

						// Get a list of Scenarios
						agent.get('/scenarios')
							.end(function(scenariosGetErr, scenariosGetRes) {
								// Handle Scenario save error
								if (scenariosGetErr) done(scenariosGetErr);

								// Get Scenarios list
								var scenarios = scenariosGetRes.body;

								// Set assertions
								(scenarios[0].user._id).should.equal(userId);
								(scenarios[0].name).should.match('Scenario Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Scenario instance if not logged in', function(done) {
		agent.post('/scenarios')
			.send(scenario)
			.expect(401)
			.end(function(scenarioSaveErr, scenarioSaveRes) {
				// Call the assertion callback
				done(scenarioSaveErr);
			});
	});

	it('should not be able to save Scenario instance if no name is provided', function(done) {
		// Invalidate name field
		scenario.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Scenario
				agent.post('/scenarios')
					.send(scenario)
					.expect(400)
					.end(function(scenarioSaveErr, scenarioSaveRes) {
						// Set message assertion
						(scenarioSaveRes.body.message).should.match('Please fill Scenario name');
						
						// Handle Scenario save error
						done(scenarioSaveErr);
					});
			});
	});

	it('should be able to update Scenario instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Scenario
				agent.post('/scenarios')
					.send(scenario)
					.expect(200)
					.end(function(scenarioSaveErr, scenarioSaveRes) {
						// Handle Scenario save error
						if (scenarioSaveErr) done(scenarioSaveErr);

						// Update Scenario name
						scenario.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Scenario
						agent.put('/scenarios/' + scenarioSaveRes.body._id)
							.send(scenario)
							.expect(200)
							.end(function(scenarioUpdateErr, scenarioUpdateRes) {
								// Handle Scenario update error
								if (scenarioUpdateErr) done(scenarioUpdateErr);

								// Set assertions
								(scenarioUpdateRes.body._id).should.equal(scenarioSaveRes.body._id);
								(scenarioUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Scenarios if not signed in', function(done) {
		// Create new Scenario model instance
		var scenarioObj = new Scenario(scenario);

		// Save the Scenario
		scenarioObj.save(function() {
			// Request Scenarios
			request(app).get('/scenarios')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Scenario if not signed in', function(done) {
		// Create new Scenario model instance
		var scenarioObj = new Scenario(scenario);

		// Save the Scenario
		scenarioObj.save(function() {
			request(app).get('/scenarios/' + scenarioObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', scenario.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Scenario instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Scenario
				agent.post('/scenarios')
					.send(scenario)
					.expect(200)
					.end(function(scenarioSaveErr, scenarioSaveRes) {
						// Handle Scenario save error
						if (scenarioSaveErr) done(scenarioSaveErr);

						// Delete existing Scenario
						agent.delete('/scenarios/' + scenarioSaveRes.body._id)
							.send(scenario)
							.expect(200)
							.end(function(scenarioDeleteErr, scenarioDeleteRes) {
								// Handle Scenario error error
								if (scenarioDeleteErr) done(scenarioDeleteErr);

								// Set assertions
								(scenarioDeleteRes.body._id).should.equal(scenarioSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Scenario instance if not signed in', function(done) {
		// Set Scenario user 
		scenario.user = user;

		// Create new Scenario model instance
		var scenarioObj = new Scenario(scenario);

		// Save the Scenario
		scenarioObj.save(function() {
			// Try deleting Scenario
			request(app).delete('/scenarios/' + scenarioObj._id)
			.expect(401)
			.end(function(scenarioDeleteErr, scenarioDeleteRes) {
				// Set message assertion
				(scenarioDeleteRes.body.message).should.match('User is not logged in');

				// Handle Scenario error error
				done(scenarioDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Scenario.remove().exec();
		done();
	});
});