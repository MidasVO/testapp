'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Scenario = mongoose.model('Scenario'),
	Test = mongoose.model('Test'),
	Request = mongoose.model('Request'),
	q = require('q'),
	_ = require('lodash');

/**
 * Create a Scenario
 */
exports.create = function(req, res) {
	var scenario = new Scenario(req.body);
	scenario.user = req.user;

	scenario.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(scenario);
		}
	});
};

/**
 * Show the current Scenario
 */
exports.read = function(req, res) {
	res.jsonp(req.scenario);
};

/**
 * Update a Scenario
 */
exports.update = function(req, res) {
	var scenario = req.scenario ;

	scenario = _.extend(scenario , req.body);

	scenario.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(scenario);
		}
	});
};

/**
 * Delete an Scenario
 */
exports.delete = function(req, res) {
	var scenario = req.scenario ;

	scenario.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(scenario);
		}
	});
};

/**
 * List of Scenarios
 */
exports.list = function(req, res) { 
	Scenario.find().sort('-created').populate('user', 'displayName').exec(function(err, scenarios) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(scenarios);
		}
	});
};

// run scenario
exports.runScenario = function (req, res) {

};
// Get all requests so we can fill the combobox
exports.getAllRequests = function (req, res) {
	Request.list();
};
//Get all tests so we can fill the combobox
exports.getAllTests = function (req, res) {
	Test.list();
};

/**
 * Scenario middleware
 */
exports.scenarioByID = function(req, res, next, id) { 
	Scenario.findById(id).populate('user', 'displayName').exec(function(err, scenario) {
		if (err) return next(err);
		if (! scenario) return next(new Error('Failed to load Scenario ' + id));
		req.scenario = scenario ;
		next();
	});
};

/**
 * Scenario authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.scenario.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
