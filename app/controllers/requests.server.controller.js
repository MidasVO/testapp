'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Request = mongoose.model('Request'),
	http = require('http'),
	requestlib = require('request'),
	_ = require('lodash');

/**
 * Create a Request
 */
exports.create = function(req, res) {
	var request = new Request(req.body);
	request.user = req.user;

	request.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * Show the current Request
 */
exports.read = function(req, res) {
	res.jsonp(req.request);
};

/**
 * Update a Request
 */
exports.update = function(req, res) {
	var request = req.request ;

	request = _.extend(request , req.body);

	request.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * Delete an Request
 */
exports.delete = function(req, res) {
	var request = req.request ;

	request.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(request);
		}
	});
};

/**
 * List of Requests
 */
exports.list = function(req, res) { 
	Request.find().sort('-created').populate('user', 'displayName').exec(function(err, requests) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(requests);
		}
	});
};

/**
 * Request middleware
 */
exports.requestByID = function(req, res, next, id) { 
	Request.findById(id).populate('user', 'displayName').exec(function(err, request) {
		if (err) return next(err);
		if (! request) return next(new Error('Failed to load Request ' + id));
		req.request = request;
		next();
	});
};

/**
 * Request authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.request.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};

exports.doBetterRequest = function (req, res) {


	requestlib({
    url: 'http://modulus.io', //URL to hit
    qs: {from: 'blog example', time: +new Date()}, //Query string data
    method: 'GET', //Specify the method
    headers: { //We can define headers too
        'Content-Type': 'MyContentType',
        'Custom-Header': 'Custom Value'
    }
}, function(error, response, body){
    if(error) {
        console.log(error);
    } else {
        console.log(response.statusCode, body);
    }
});
};

/**
 * Do the request
 */
 exports.doRequest = function(req, res) {
 	var request = req.request, thisParamName, thisParamValue, ourParam;
 	 	var params = {}, options;
 	 	var testMethod = 'GET';
 	 	// Create querystring:
 	 	for (var x = 0; x < request.parameters.length; x++) {
 	 		thisParamName = request.parameters[x].name;
 	 		thisParamValue = request.parameters[x].value;
 	 		ourParam = thisParamName + ':'+ thisParamValue;
 	 		params[thisParamName] = thisParamValue;
 	 	}

			if(testMethod === 'POST') {
 	 		 	options = {
		 		hostname: request.hostname,
		 		port: request.port,
		 		path: request.path,
		 		method: 'POST',
		 		form: params
		 	};
		 	} else {
 	 		 	options = {
		 		hostname: request.hostname,
		 		port: request.port,
		 		path: request.path,
		 		method: 'GET',
		 		qs: params

		 	};
		 }

 	var body = '';

 	var dataObj;
 	var req1 = http.request(options,function (response) {
 		var dataObj = {
 			statusCode : response.statusCode,
 			body : '',
 			headers: ''
 		}; //eval om de expressen te doen
 		console.log('STATUS: ' + response.statusCode);
 		console.log('HEADERS: ' + JSON.stringify(response.headers));
 		var headers = JSON.stringify(response.headers);
 		dataObj.headers = headers;
 		console.log('responseText: ' + response.responseText);
 		response.setEncoding('utf8');
 		response.on('data', function (chunk) {
 			console.log('BODY: ' + chunk);
 			body += chunk;
 		});
 		response.on('end', function() {
 			console.log('on end data: ' + body);
 			dataObj.body = body;
 			res.send(dataObj);
 		});
 	}).on('error', function (e) {
 		console.log('problem with request: ' + e.message);
 	});
 	req1.end();
 };