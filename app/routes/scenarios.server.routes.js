'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var scenarios = require('../../app/controllers/scenarios.server.controller');

	// Scenarios Routes
	app.route('/scenarios')
		.get(scenarios.list)
		.post(users.requiresLogin, scenarios.create);

	app.route('/scenarios/:scenarioId')
		.get(scenarios.read)
		.put(users.requiresLogin, scenarios.hasAuthorization, scenarios.update)
		.delete(users.requiresLogin, scenarios.hasAuthorization, scenarios.delete);

	// Finish by binding the Scenario middleware
	app.param('scenarioId', scenarios.scenarioByID);
};
