'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request Schema
 */
var RequestSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Request name',
		trim: true
	},
	port: {
		type: Number,
		default: 3000,
		required: 'Please fill Port number',
		trim: true
	},	
	description: {
		type: String,
		default: '',
		required: 'Please fill a description',
		trim: true
	},
	path: {
		type: String,
		default: '/',
		required: 'Please fill Path',
		trim: true
	},
	hostname: {
		type: String,
		default: '',
		required: 'Please fill Hostname',
		trim: true
	},
	method: {
		type: String,
		required: 'Please fill in a request method'
	},
	parameters: [],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Request', RequestSchema);