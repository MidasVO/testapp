'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Scenario Schema
 */
var ScenarioSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Scenario name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	sequence: {
		type: Number,
		required: 'Please fill in a sequence'
	},
	testSet: [],

	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Scenario', ScenarioSchema);