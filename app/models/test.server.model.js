'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Test Schema
 */
var TestSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Test name',
		trim: true
	},
	expression: {
		type: String,
		default: '',
		required: 'Please fill an expression',
		trim: true
	},
	operator: {
		type: String,
		default: '',
		required: 'Please fill an operator',
		trim: true
	},
	expectation: {
		type: String,
		default: '',
		required: 'Please fill an expectation',
		trim: true
	},
	description: {
		type: String,
		default: '',
		required: 'Please fill a description',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	},
	request: {
		type: Object
	}
});

mongoose.model('Test', TestSchema);