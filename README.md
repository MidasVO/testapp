About
-----
A single page application which lets you test RESTful API's. Create requests, assertions, and scenario's.

Install
-----
1. Clone the repository
2. npm install
3. npm install q
4. npm install request
5. grunt


Importing the database
-----------

`mongorestore testapp-dev